source 'https://rubygems.org'

# Full-stack web application framework. (http://www.rubyonrails.org)
gem 'rails', '~>4.1.0'

# Pg is the Ruby interface to the {PostgreSQL RDBMS}[http://www.postgresql.org/] (https://bitbucket.org/ged/ruby-pg)
gem 'pg'
# Makes http fun! Also, makes consuming restful web services dead easy. (http://jnunemaker.github.com/httparty)
gem 'httparty'
# New Relic Ruby Agent (http://www.github.com/newrelic/rpm)
gem 'newrelic_rpm'
# Integrate Compass into Rails 3.0 and up. (https://github.com/Compass/compass-rails)
gem "compass-rails"
# rubyist way to render variant chart by highcharts without write javascript right now, rails gem library. (https://github.com/michelson/lazy_high_charts)
gem 'lazy_high_charts'
# D3 for Rails Asset Pipeline (https://github.com/iblue/d3-rails)
gem "d3-rails", :git => "https://github.com/demiazz/d3-rails"
# Twitter's Bootstrap, converted to Sass and ready to drop into Rails or Compass (http://github.com/thomas-mcdonald/bootstrap-sass)
gem 'bootstrap-sass', '~> 2.3.2.0'
# An elegant, structured (X)HTML/XML templating engine. (http://haml.info/)
gem "haml"
# Angular.js UI Bootstrap on Rails (https://github.com/cconstantin/angular-ui-bootstrap-rails/)
gem 'angular-ui-bootstrap-rails'
# Summary: Use ngannotate in the Rails asset pipeline. (https://github.com/kikonen/ngannotate-rails)
gem 'ngannotate-rails'
# The simplest way to group temporal data (https://github.com/ankane/groupdate)
gem 'groupdate'

# Simple, efficient background processing for Ruby (http://sidekiq.org)
gem 'sidekiq'
# Sidekiq Cron helps to add repeated scheduled jobs (http://github.com/ondrejbartas/sidekiq-cron)
gem "sidekiq-cron", "~> 0.4.0"
# For Sidekiq::Web
gem 'sinatra', :require => nil

# ScrApify scraps static html sites to scraESTlike APIs (http://www.github.com/sathish316/scrapify)
gem 'scrapify'

# Bower for Rails (https://github.com/rharriso/bower-rails)
gem 'bower-rails'
# Use your angular templates with rails' asset pipeline (https://github.com/pitr/angular-rails-templates)
gem 'angular-rails-templates'

group :development do
  # Better error page for Rails and other Rack apps (https://github.com/charliesome/better_errors)
  gem "better_errors"
  # Retrieve the binding of a method's caller. Can also retrieve bindings even further up the stack. (http://github.com/banister/binding_of_caller)
  gem "binding_of_caller"
  # Use Pry as your rails console (https://github.com/rweng/pry-rails)
  gem 'pry-rails'
  # This module allows Ruby programs to interface with the SQLite3 database engine (http://www.sqlite.org) (https://github.com/sparklemotion/sqlite3-ruby)
  gem 'sqlite3'
  # Turns off Rails asset pipeline log. (http://github.com/evrone/quiet_assets)
  gem 'quiet_assets'
end

group :production do
  # Following best practices from http://12factor.net run a maintainable, clean, and scalable app on Rails (https://github.com/heroku/rails_12factor)
  gem "rails_12factor"
end

# Rack-based asset packaging system (http://getsprockets.org/)
gem 'sprockets', '2.11.0' # 2.12.0 is broken
# Sass adapter for the Rails asset pipeline. (https://github.com/rails/sass-rails)
gem 'sass-rails',   '~> 4.0.0'
# CoffeeScript adapter for the Rails asset pipeline. (https://github.com/rails/coffee-rails)
gem 'coffee-rails', '~> 4.0.0'
# Ruby wrapper for UglifyJS JavaScript compressor (http://github.com/lautis/uglifier)
gem 'uglifier', '>= 1.0.3'

# Use jQuery with Rails 3+ (http://rubygems.org/gems/jquery-rails)
gem 'jquery-rails'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# A thin and fast web server (http://code.macournoyer.com/thin/)
gem 'thin'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'

# Protect attributes from mass assignment in Active Record models (https://github.com/rails/protected_attributes)
gem 'protected_attributes' # https://github.com/rails/protected_attributes
# REST modeling framework (part of Rails). (http://www.rubyonrails.org)
gem 'activeresource' # https://github.com/rails/activeresource
# Action caching for Action Pack (removed from core in Rails 4.0) (https://github.com/rails/actionpack-action_caching)
gem 'actionpack-action_caching' # https://github.com/rails/actionpack-action_caching
# An Action Dispatch session store backed by an Active Record class. (http://www.rubyonrails.org)
gem 'activerecord-session_store' # https://github.com/rails/activerecord-session_store
# ActiveModel::Observer, ActiveRecord::Observer and ActionController::Caching::Sweeper extracted from Rails. (https://github.com/rails/rails-observers)
gem 'rails-observers' # https://github.com/rails/rails-observers
# Note that there might be more functionalities that were extracted
