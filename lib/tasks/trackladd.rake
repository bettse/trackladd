namespace :trackladd do
  desc "Scrape prices for today"
  task :scrape => :environment do
    puts "Moved to Scrape class (app/jobs/scrape.rb)"
  end

  desc "Pulls units from old db"
  task :importunits => :environment do
    require 'sqlite3'
    db = SQLite3::Database.open "ladd.db"
    db.results_as_hash = true
    rows = db.execute "SELECT * FROM Units"

    rows.each do |row|
      Unit.create(:unitnumber => row['unitnumber'], :bedrooms => row['bedrooms'], :bathrooms => row['bathrooms'], :sqft => row['sqft'], :den => row['den'], :studioplus => row['studioplus'])
    end

    db.close if db

  end

  desc "Pulls prices from old db"
  task :importprices => :environment do
    require 'sqlite3'
    db = SQLite3::Database.open "ladd.db"
    db.results_as_hash = true
    rows = db.execute "SELECT * FROM Prices"

    rows.each do |row|
      Price.create(:unitnumber => row['unitnumber'], :price => row['price'], :created_at => Time.at(row['date']).to_datetime)
    end
    db.close if db
  end
end
