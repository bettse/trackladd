require 'sidekiq/web'
require 'sidekiq/cron/web'

Backend::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  mount Sidekiq::Web => '/sidekiq'

  get 'prices/:number/bedrooms', to: 'prices#bedrooms'
  get 'prices/average', to: 'prices#average'

  resources :prices
  resources :units, except: [:edit]

  root to: 'units#index'

  #The ladd flash requests a views.xml and I want to prevent this from being a 404
  get 'views', to: 'application#views'
end
