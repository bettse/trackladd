# Load the Rails application.
require File.expand_path('../application', __FILE__)

require File.join(Rails.root,'lib/regression.rb')

# Initialize the Rails application.
Backend::Application.initialize!
