# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Backend::Application.config.secret_key_base = 'c42b405828bd59a02d63864127e1b38775fcadd8f361afd1b495ae44979c637d8becbac4885cf89da76fcf080f00a5ce3fd0ffe419338ee0cbb2ef6568cc8b6d'
