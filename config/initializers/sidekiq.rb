#Redis set using REDIS_URL env var

schedule_file = Rails.root.join('config', 'schedule.yml')

if Sidekiq.server? && File.exists?(schedule_file)
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
end
