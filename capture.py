#!/usr/bin/python

import requests
from pprint import pprint
import sqlite3
import time
import json
import sys

#define constants
date = int(time.time())
query = "insert into prices (unitnumber, price, date) values (?, ?, ?);"

#DB setup
conn = sqlite3.connect('ladd.db')
c = conn.cursor()

#Grab data
r = requests.get('http://apify.heroku.com/api/laddapts.json')
unitlist = json.loads(r.content)

#Cleanup price and insert
for unit in unitlist:
    #headerrow
    if(unit['bedrooms'] == 'Bed'): continue
    price = unit['price'].replace('$', '').replace(',', '')
    #Handle ranges
    if(price.find('-') > -1):
        low, high = price.split('-')
        price = low.strip()
    # Insert a row of data
    c.execute(query, (unit['unitnumber'], price, date))

# Save (commit) the changes
conn.commit()

# We can also close the cursor if we are done with it
c.close()
