
n.n.n / 2014-05-31 
==================

 * Adding history created by git-extras' 'git changelog'
 * daily updates
 * Prevents casting of null prices to 0, which was creating inaccurate charts
 * daily update
 * Merge pull request #1 from bettse/bettse/rails4
 * Create scope for common sql; correct js cast of price data
 * Updating hash syntax to ruby 1.9
 * Make copyright year dynamic
 * Upgrade to rails 4
 * bundle update
 * Formatting readme
 * Merge branch 'master' of github.com:bettse/trackladd What the hell?
 * daily updates
 * Create LICENSE.md
 * upgrade ruby version
 * Move some logic from controller to model; need to check if its still performant
 * rearrange haml id/class order
 * Add vim swp to gitignore
 * new hash syntax
 * new hash syntax
 * new hash syntax
 * break linear regression into method
 * Add CodeClimate to readme
 * daily updates
 * Clarify time ranges
 * daily updates
 * Trying to fix heroku deploy
 * Security: update to ruby 2.1.0 and rails 3.2.17
 * Add zeus
 * daily updates
 * daily updates
 * update json
 * daily update
 * daily update
 * More custom NR
 * new units/history view with d3 scatter
 * Doesn't autoload history charts; indentation
 * add rails_12factor to gemfile
 * NR instrumentation
 * daily updates
 * Update favicon
 * Improve index#prices
 * daily updates
 * try out custom metric
 * Revert "removing normal nr in favor of heroku nr"
 * whitespace cleanup
 * daily updates
 * removing normal nr in favor of heroku nr
 * dialy updates
 * daily updates
 * comment out logger in scrape task
 * daily updates
 * Handle no data more gracefully
 * Daily updates
 * simplify array creation
 * add pry to development
 * change puts to logger
 * adding pry
 * auto create units
 * update d3
 * ignore .tags
 * daily updates
 * daily updates
 * daily updates
 * daily update
 * daily updates
 * daily update
 * daily update
 * Updating Gemfile.lock
 * Adding better_errors in development mode
 * log scale for price scatter
 * daily updates
 * remove some d3 charts
 * tooltips on value chart
 * change price hisotry chart to not include 0
 * Add titles to charts, correct top margin to fit titles
 * Reindent js files
 * Add title option to all d3 charts
 * daily update
 * breaking graphs into their own functions(ish)
 * Value chart in d3
 * daily updates
 * Move unit specific price back into index method, roll average_all into average
 * improve missing method
 * remove logging
 * Change from find to find_all_by_unitnumber so missing units dont cause an error on available
 * correct missing method
 * daily updates
 * remove sublime project
 * removing webui and mobileui
 * Split line when null detected for price
 * colorize and size dots on all prices chart based on bedroom count and sqft respectively
 * Add thead/tbody to available table
 * daily updates
 * rotate x axis labels
 * D3 scatterplot of all prices
 * Moved former price#index to price#average, rewrote price#index to its original version returning all prices
 * Add prices chart using d3 to main page
 * Update d3
 * Add d3
 * New if only one price or no price yesterday
 * daily updates
 * Drop moveinspecials table from ladd.db
 * add 305 to ladd.db
 * Add missing datum
 * remove id from units table and make unitnumber primary key
 * correct path to ladd.db in rake task
 * daily updates
 * daily updates
 * remove unused stuff relaed to home controller
 * Change index to use units#available
 * Added simple lienar regression to value chart
 * Hook ui buttons into loading data
 * Add buttons and textfields for more price history types
 * endpoint for average price by number of bedrooms
 * remove trend column, only show badge if a non-zero trend exists, show badge next to current price
 * update favicon
 * daily updates
 * Improve price chart tooltip
 * correct sort order
 * Change from inserting nulls once per month to inserting null prices for all missing dates
 * Chagne to using covers method for checking date range
 * Add meta viewport for bootstrap responsive
 * Recorrect modal fix
 * Remove unused view templates
 * Remove unused methods, reduce number of sql queries
 * Add up to 12 null price dates within the range of known dates for prices
 * daily updates
 * Dress up info link
 * div and javascript for displaying missing units
 * Load unit info into modal
 * modal div structure, wired into Info link
 * Move units#available to using bootstrap badge +icon, remove circle images and nzp method
 * Switch home#index from pngs for trend to bootstrap icons + badges
 * Links for jumping to anchors on mobile
 * Remove desktop and mobile css, add navbar to home#index
 * add table, charts to home#index
 * Move code from application to home.js
 * removing desktop (extjs ui)
 * redirect mobile to index
 * remove mobile (sencha touch ui)
 * Load history chart with average of all units to start with
 * daily updates
 * convert application.html to haml
 * New method nzp.  Convienience method that returns one of 3 parameters strings depending on if the value is negative, zero, or positive
 * rewrote bedrooms,index, show for units to haml
 * rewrite home/index home/mobile to haml
 * Add Haml, rewrite price/index to haml
 * daily update
 * Change available page price chart to highstock
 * Mark prices highstock as ordinal:false to correct for iregular date data intervals
 * Add js to listen for row clicks and load unit data for clicked row
 * daily updates
 * correct calls to price method to current_price and new? to new
 * correct indenting
 * Move trend, new, price into unit controller; busts number of queries from 8 per unit to 2 per unit
 * Add new price history chart that is loaded when a point on value chart is clicked
 * daily update
 * code formatting to reduce long lines
 * Add bedroom count to pie label
 * Got rid of loader error in development by ordering requires directives correctly
 * Moving ext.loader into desktop.js
 * daily updates
 * testing ext.loader configs
 * trying to get desktop version working by making more similar to working mobile version
 * Trying something else; adding precompile of desktop/mobile to app config
 * Move highstock/highcharts-more into application.js
 * switch off compile in production; remove precompile regex
 * compile assets in production, regex for precompile
 * move highcharts precompile into app config
 * Revert "Moving extjs app from using directives to include tags"
 * Moving extjs app from using directives to include tags
 * adding newrelic config
 * Switch to unicorn
 * Cast unit.count to integer when handing to highcharts
 * fix bedrooms N+1
 * daily updates
 * add highcharts to precompile for production
 * add highcharts to precompile for production
 * set ruby version
 * remove sqlite3 from Gemfile
 * Migrate over to postgres
 * new readme
 * MASSIVE CHANGE: move rails into root of repo
 * Add link to prices on available page
 * Convert price history to use highstock
 * Move value chart from rails controller to javascript
 * remove gauge chart
 * value (price distribution) chart
 * daily updats
 * Add gauge chart of occupancy
 * allowPointSelect: true on bedrooms pie chart
 * reorder methods in units controller, add variable to add to hero text (all, missing, available)
 * add current price to available, sorting
 * color dots for trend
 * Style unit#index more like available grid in extjs
 * Add toggleZero feature to price history; remove limit on number of points graphed
 * fix floorplan image url for floors [6,22]
 * use index template for both index and available units
 * revamp index, add show link to index and available
 * Add back a show page for unit details
 * move javascript include tags into context_for
 * add bootstrap to available and bedrooms
 * add unitnumber to hero text
 * proper formatting for currency
 * more formatting, money formatting, hero text, title of chart
 * bootstrap tables
 * Adding bootstrap
 * Change from int zero to float 0.0
 * daily updates
 * bold new nunits in both desktop and mobile
 * pie chart of bedrooms distributions on /units/bedrooms
 * corrected typo in available template and removed endpoints that don't exist anymore
 * Add lazy highcharts and chart unit prices on the /unit/x/prices page
 * look up unit if enter is hit in unitnumber textfield
 * show floorplan for looked up units
 * daily updates
 * Trying out sublime text
 * Got my head out of my ass and got loadHistoryByNumber working
 * move mobile-app include to mobile.js directive
 * move app.js include to directive in desktop.js
 * Move togglezero handler into controller; try to add lookup for units
 * Allow setting of created_at of price since its used in db:seed
 * Add index to price and unit tables
 * Reindent to 2 spaces
 * Fill out db seed file with delete of all data and reimport using trackladd tasks
 * rename tasks to import...
 * adding unit 2209
 * clean up scaffold created files
 * daily updates
 * change new() to create()
 * Updates to support sencha touch 2.2
 * daily updates
 * correct variable used to set Price's price
 * correctios to price and trend
 * Correct timezone
 * comment out setItems of slideshow
 * Added Trend as a store and view as a way of fixing it
 * Place sass into scss file
 * Rename RentHistory model to Price, create Trend store
 * correct more img urls
 * Got mobileui working with errors, broke up javascript so only loaded on / and /mobile for two uis
 * Add js requires and scss for sencha-touch-rails
 * adding github.com/methyl/sencha-touch-rails to Gemfile
 * adding /mobile page
 * Only create extjs app on home/index
 * redraw graph on completion of store load
 * Clean up and support returning every nth price
 * Adding Missing grid, hidden until unit is missing
 * reduce width of available grid, rotate bottom labels of rent history
 * available grid: flex trend column, adjust image height
 * emptytext for grid
 * Limit prices to last 12 months
 * Show average prices when no unit specified
 * improve queries
 * endpoint for missing units
 * Added bedrooms distribution endpoint to backend, updated frontend url
 * big one: rename renthistory model/store to price; got query for prices working poorly
 * Correct query to average prices
 * Price index is average prices
 * remove linebreaks and print statement from task
 * refactor unit model methods
 * Fix price and date format
 * Add price as attribute of unit
 * first set of changes to support backend
 * Add desktop app to backend; ui shows, no data currently
 * Update title in application template
 * Adding extjs-rails gem
 * Add newrelic
 * Add trend and new to json for unit
 * Add new to unit model
 * Add trend to Unit model
 * Add available method in Units
 * Added home controller
 * Update primary key syntax
 * Added bin to gitignore
 * gemfile.lock
 * Add task that scrapes prices
 * Update gitignore using github rails default
 * Adding httpparty to gemfile
 * renamed fromold to trackladd; added method for importing prices; added link between units and prices
 * task for importing units from old db
 * new rails + models for user and price
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * update gitignore with build and archive
 * update debug.html
 * daily updates
 * Comment out line that throws error
 * tweak
 * Update build.sh
 * Upgrading to Sencha Touch 2.2.1
 * Upgrading to Sencha Touch 2.2.1
 * removing moveinspecial from views in controller
 * daily update
 * Remove unused specials model,store, view
 * daily updates
 * further remove specials since they aren't used any longer
 * update schema file
 * removing backend to refactor first
 * daily updates
 * add prices to show unit json
 * sort prices by date
 * table of prices when showing unit
 * Further corrections to associations, remove need for 3 migrations
 * adding unit-price relationships
 * Followed first steps in guide; created units and prices models; added migrations to support importing data from old db
 * commiting newly created RoR backend
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * LImit rent prices to the last year
 * daily updates
 * point index to ext 4.1.0
 * daily updates
 * Average over week instead of over day
 * daily updates
 * default to not including 0 in rent history
 * daily updates
 * correct capacity to remove model units
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * IDEAS
 * Removing outline around floorplan images
 * Removing outline around floorplan images
 * daily updates
 * crop images to remove sqft/number
 * crop images to remove sqft/number
 * nes carousel view of available units...rent history in cariusel view broken
 * daily updates
 * increase overlay height
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily update
 * if non-3d, show label
 * add more ideas
 * daily updates
 * Add style->renderer, although I'm not sure how to use it yet
 * dialy updates
 * dialy updates
 * daily updates
 * Cleanup and simplify capture script by using apify.heroku.com
 * remove capturing of specials since there aren't going to be any
 * change badge on refilter; remove completed ideas from ideas files, add in old ones that were lost during upgrade
 * Add panel (starts hidden) that shows floorplan when unit selected
 * Add floorplan files to webui
 * move around charts
 * daily updates
 * review valuegrad from price distro
 * correct how renthistory max is set so all instances are set
 * Revert "set max of rent history to 10% more than highest price in store"
 * 2205 + daily updates
 * set max of rent history to 10% more than highest price in store
 * remove specials enabling since there aren't any
 * daily updates
 * Adding routes
 * hide things that don't work
 * ui tweaks
 * rotate labels
 * iphone view of pie and gauge charts
 * a bunch of chart interactions
 * colorize price distribution with value gradient
 * readd unitdetail to pricedistribution chart
 * show image larger when tapped
 * iphone view
 * improve method for setting unit details
 * daily updates
 * hook up include zero button, but currently not working
 * dialy updates
 * remove Trend and use RentHistory in its place
 * increase max for rent history and trend charts
 * a little controller cleanup, and setting max to be 10% higher than max of store for price distribution chart
 * daily updates
 * Fix RentHistory
 * Fix trend chart
 * Fixed Price Distribution chart
 * cleanup and making gauge a little better looking
 * fixing capacity gauge and adding titlebars
 * comment out broken lines
 * Added alias to store, not sure if it matters
 * Fixed bedroompie
 * Trying to fix Trend chart; shows axes
 * remove watermark
 * comment out setting of store for chart that is currently broken
 * change 'new' type to integer
 * [Major] Upgrading from Sencha touch 2.0.0 to sench touch 2.1; charts currently broken
 * remove guage axes that no longer exists in sencha touch 2.1
 * correct capitalization
 * ideas
 * daily updates
 * add 312
 * daily updates
 * daily updates
 * remove completed idea
 * try to fill in the 6 through 22 floors of units
 * change from hide/show of specials to enable/disable
 * daily updates
 * daily updates + units
 * more trend cleanup
 * daily updates
 * Add buttons for includezero, nonfunctional
 * color best value unit green
 * add Trend.js to build.sh
 * new ideas
 * Replace Trend with renthistory of all
 * Replace Trend with renthistory of all
 * Add yearly trend to mobileui; correct special showing code
 * daily updates
 * Hide specials by default, show if non-empty
 * shorten renthistory chart
 * order mobileui list by price
 * New average unit price graph (trend)
 * daily updates
 * daily updates
 * sort by price
 * Add new button to toggle 0 as the Rent History minimum
 * Move RentHistory chart into a panel within the class
 * Flex price distribution
 * Add unit 907 to db
 * Little less wide, hide specials if empty store
 * Daily update
 * Limit number of returned results for rent history
 * daily updates
 * Add css folder and sass cache to ignore
 * Change to styling icons using css, add price to list
 * perm change
 * perm change
 * add compass compile to build script
 * remove app.css since its compiled
 * Fold specials css into app.scss
 * new units
 * Correct math used to calculate floor that unit is on
 * daily updates
 * Correct setting title of avilable list when there are missing units
 * 516 and daily updates
 * daily updates
 * daily updates and a new unit
 * daily + more units
 * unit update and dailies
 * correct typo
 * daily updates
 * daily updates
 * daily updates, 3 new units
 * increase qtip width
 * Adding more units, more updates
 * adding 614 and a couple days of updates
 * Add logic to capture script to handle price ranges
 * New units and a bunch of daily updates.  This is the first update after the list was empty for a couple days, and it came back with range prices instead of specific prices
 * More units, daily updates
 * daily updtes and a couple new units
 * remove 0 from pricedistribution minimum
 * adding more units and daily updates
 * updates from a few days + a few new units
 * database updates
 * make sql more explicit
 * daily update + 1712, 1906
 * remove rent slider button
 * updates from the last few days
 * Added 1512 and daily update
 * daily update
 * daily update
 * correct new
 * adding 1510
 * remove floorplan from itemdetail panel off price distribution scatter plot
 * remove implemented price filter idea
 * remove pie chart legend, improve itemdetail panel
 * Change available units to use a left join to allow units without a trend, untested
 * update db
 * daily update
 * make sql datetime localtime
 * build file perm
 * correct unit sorting
 * make SQL more native
 * add explicit return false
 * typo
 * containers displaying min/max of slider
 * name rent sliderfield
 * Make slider functional
 * daily update
 * remove console log
 * style trend in price distribution chart using symbols
 * Currently nonfunctional rent range button/slider
 * remove completed idea
 * Color dot red if new
 * Add (new) next to unit numbers that are new
 * file of feature ideas
 * improve query
 * remove old query
 * daily update
 * daily update
 * stuff for doing a minified build
 * rearrange order of tabs
 * daily update
 * daily update
 * Add 'all' filter button and add in controller code
 * Correct unit sorting
 * adding 1207 and 1014
 * Add segmented buttons for filtering bedrooms, not implemented yet
 * add new units
 * added 2216, 1914, 1001
 * Add floorplans to studio/studio+
 * adding 1106 and 1112
 * put floorplan to right of data in template
 * add 1201 and 1902
 * correct title
 * move the navigation view into Available
 * Correct template logic, update ladd db
 * ladd.db
 * new floorplan files, update gitignore
 * move total units into config.Constants
 * add minimum to sqft of price chart
 * panzoom rent history
 * panzoom price chart
 * add iteminfo to rent history
 * Correct renthistory model so chart shows correctly
 * Add crappy line chart to unit details page
 * set title of price chart infoitem
 * correct perms in resources
 * Add value to unitdetails, add unitdetails to iteminfo in price chart
 * move unitdetail template into Constants
 * add floorplan to unit detail
 * improve price distribution axis
 * improve iteminfo for price distribution
 * price scatter plot
 * improve iteminfo
 * Correct capacity guage value
 * correct locaiton of pie interactions
 * Add guage chart
 * Move bedroompie into half of container
 * interactions to pie chart
 * Add rendered labels
 * pie chart of bedrooms
 * Add charts css/js to index.html
 * height of arrows
 * placeholder tabs for charts
 * Correct index title and add title to move in specials tab
 * full year in unitdetails
 * Mark renthistory as remotesort
 * Sort renthistory using DB
 * Add historic prices to unit details page
 * Break unitdetails into 2 containers so I can add in historical prices as well
 * Message when there are no specials, css for specials
 * Badge Units tab with number of available units
 * align images in available list
 * typo, correct bedroom printing logic
 * nonworking code to add asterisk to title if some units are missing
 * Better unit details layout
 * basic data to details page whlie I work on layout
 * got details page to open
 * Correct avaialble xtemplate
 * typo
 * Adding constructor to controller
 * Improve look of gauge chart
 * donut gauge
 * gauge of ladd capacity
 * Trying to xtemplate the list items, buggy itemtap
 * Frist attempt at selecting items in list
 * Remove video
 * Fill out move in special
 * Loads available units into list, sorts
 * Correct urls in proxys
 * config section for models
 * typo
 * Add config section
 * Correct some names
 * Correct model names in stores
 * Adding mobileui...was working...not right now
 * padding and a little wider
 * remove log
 * better labeling
 * expand size of page
 * Clicking on scatter will higlight item on grid
 * Adjust widths and heights
 * Adjust widths and heights
 * css for moveinspecial
 * Add most recent move in special to blank space
 * tighten code for bedroom renderer, add commented out 'w/ den'
 * tooltip of price in renthistory chart
 * RentHistory on select as well as itemclick
 * Pull the number of missing items and add asterisk to available grid title if non zero
 * pull studioplus from db, use in bedrooms column to label
 * Print 'studio' for 0 bedroom units
 * new page for finding units without unit table data
 * Correct bedrooms distributon sql
 * Add price per sqft to price distribution tooltip
 * Add tooltips to trend dots with exact price change
 * correct trend subtraction
 * limit viewport to 1024x768
 * Fix some grid issues
 * Add tip with unit number
 * Improve layout
 * Add grid column with colored circle to represent price change
 * New data for price trend
 * Add count to bargraph
 * Add protection against missing move in special
 * Better layout
 * Interactiveness so clicking item in grid plots its history in chart
 * php, model, store for rent history
 * scatter plot of price per sqft
 * Adding chart of bedroom distribution
 * Web ui with a grid of today's prices
 * code to add scaped data to sqlite db
 * Change schema from sqlitedb instance to text of the schema
 * Adding gitignore
 * Adding db schema
 * Rm database comments
 * initial commit

n.n.n / 2014-05-31 
==================

 * daily updates
 * Prevents casting of null prices to 0, which was creating inaccurate charts
 * daily update
 * Merge pull request #1 from bettse/bettse/rails4
 * Create scope for common sql; correct js cast of price data
 * Updating hash syntax to ruby 1.9
 * Make copyright year dynamic
 * Upgrade to rails 4
 * bundle update
 * Formatting readme
 * Merge branch 'master' of github.com:bettse/trackladd What the hell?
 * daily updates
 * Create LICENSE.md
 * upgrade ruby version
 * Move some logic from controller to model; need to check if its still performant
 * rearrange haml id/class order
 * Add vim swp to gitignore
 * new hash syntax
 * new hash syntax
 * new hash syntax
 * break linear regression into method
 * Add CodeClimate to readme
 * daily updates
 * Clarify time ranges
 * daily updates
 * Trying to fix heroku deploy
 * Security: update to ruby 2.1.0 and rails 3.2.17
 * Add zeus
 * daily updates
 * daily updates
 * update json
 * daily update
 * daily update
 * More custom NR
 * new units/history view with d3 scatter
 * Doesn't autoload history charts; indentation
 * add rails_12factor to gemfile
 * NR instrumentation
 * daily updates
 * Update favicon
 * Improve index#prices
 * daily updates
 * try out custom metric
 * Revert "removing normal nr in favor of heroku nr"
 * whitespace cleanup
 * daily updates
 * removing normal nr in favor of heroku nr
 * dialy updates
 * daily updates
 * comment out logger in scrape task
 * daily updates
 * Handle no data more gracefully
 * Daily updates
 * simplify array creation
 * add pry to development
 * change puts to logger
 * adding pry
 * auto create units
 * update d3
 * ignore .tags
 * daily updates
 * daily updates
 * daily updates
 * daily update
 * daily updates
 * daily update
 * daily update
 * Updating Gemfile.lock
 * Adding better_errors in development mode
 * log scale for price scatter
 * daily updates
 * remove some d3 charts
 * tooltips on value chart
 * change price hisotry chart to not include 0
 * Add titles to charts, correct top margin to fit titles
 * Reindent js files
 * Add title option to all d3 charts
 * daily update
 * breaking graphs into their own functions(ish)
 * Value chart in d3
 * daily updates
 * Move unit specific price back into index method, roll average_all into average
 * improve missing method
 * remove logging
 * Change from find to find_all_by_unitnumber so missing units dont cause an error on available
 * correct missing method
 * daily updates
 * remove sublime project
 * removing webui and mobileui
 * Split line when null detected for price
 * colorize and size dots on all prices chart based on bedroom count and sqft respectively
 * Add thead/tbody to available table
 * daily updates
 * rotate x axis labels
 * D3 scatterplot of all prices
 * Moved former price#index to price#average, rewrote price#index to its original version returning all prices
 * Add prices chart using d3 to main page
 * Update d3
 * Add d3
 * New if only one price or no price yesterday
 * daily updates
 * Drop moveinspecials table from ladd.db
 * add 305 to ladd.db
 * Add missing datum
 * remove id from units table and make unitnumber primary key
 * correct path to ladd.db in rake task
 * daily updates
 * daily updates
 * remove unused stuff relaed to home controller
 * Change index to use units#available
 * Added simple lienar regression to value chart
 * Hook ui buttons into loading data
 * Add buttons and textfields for more price history types
 * endpoint for average price by number of bedrooms
 * remove trend column, only show badge if a non-zero trend exists, show badge next to current price
 * update favicon
 * daily updates
 * Improve price chart tooltip
 * correct sort order
 * Change from inserting nulls once per month to inserting null prices for all missing dates
 * Chagne to using covers method for checking date range
 * Add meta viewport for bootstrap responsive
 * Recorrect modal fix
 * Remove unused view templates
 * Remove unused methods, reduce number of sql queries
 * Add up to 12 null price dates within the range of known dates for prices
 * daily updates
 * Dress up info link
 * div and javascript for displaying missing units
 * Load unit info into modal
 * modal div structure, wired into Info link
 * Move units#available to using bootstrap badge +icon, remove circle images and nzp method
 * Switch home#index from pngs for trend to bootstrap icons + badges
 * Links for jumping to anchors on mobile
 * Remove desktop and mobile css, add navbar to home#index
 * add table, charts to home#index
 * Move code from application to home.js
 * removing desktop (extjs ui)
 * redirect mobile to index
 * remove mobile (sencha touch ui)
 * Load history chart with average of all units to start with
 * daily updates
 * convert application.html to haml
 * New method nzp.  Convienience method that returns one of 3 parameters strings depending on if the value is negative, zero, or positive
 * rewrote bedrooms,index, show for units to haml
 * rewrite home/index home/mobile to haml
 * Add Haml, rewrite price/index to haml
 * daily update
 * Change available page price chart to highstock
 * Mark prices highstock as ordinal:false to correct for iregular date data intervals
 * Add js to listen for row clicks and load unit data for clicked row
 * daily updates
 * correct calls to price method to current_price and new? to new
 * correct indenting
 * Move trend, new, price into unit controller; busts number of queries from 8 per unit to 2 per unit
 * Add new price history chart that is loaded when a point on value chart is clicked
 * daily update
 * code formatting to reduce long lines
 * Add bedroom count to pie label
 * Got rid of loader error in development by ordering requires directives correctly
 * Moving ext.loader into desktop.js
 * daily updates
 * testing ext.loader configs
 * trying to get desktop version working by making more similar to working mobile version
 * Trying something else; adding precompile of desktop/mobile to app config
 * Move highstock/highcharts-more into application.js
 * switch off compile in production; remove precompile regex
 * compile assets in production, regex for precompile
 * move highcharts precompile into app config
 * Revert "Moving extjs app from using directives to include tags"
 * Moving extjs app from using directives to include tags
 * adding newrelic config
 * Switch to unicorn
 * Cast unit.count to integer when handing to highcharts
 * fix bedrooms N+1
 * daily updates
 * add highcharts to precompile for production
 * add highcharts to precompile for production
 * set ruby version
 * remove sqlite3 from Gemfile
 * Migrate over to postgres
 * new readme
 * MASSIVE CHANGE: move rails into root of repo
 * Add link to prices on available page
 * Convert price history to use highstock
 * Move value chart from rails controller to javascript
 * remove gauge chart
 * value (price distribution) chart
 * daily updats
 * Add gauge chart of occupancy
 * allowPointSelect: true on bedrooms pie chart
 * reorder methods in units controller, add variable to add to hero text (all, missing, available)
 * add current price to available, sorting
 * color dots for trend
 * Style unit#index more like available grid in extjs
 * Add toggleZero feature to price history; remove limit on number of points graphed
 * fix floorplan image url for floors [6,22]
 * use index template for both index and available units
 * revamp index, add show link to index and available
 * Add back a show page for unit details
 * move javascript include tags into context_for
 * add bootstrap to available and bedrooms
 * add unitnumber to hero text
 * proper formatting for currency
 * more formatting, money formatting, hero text, title of chart
 * bootstrap tables
 * Adding bootstrap
 * Change from int zero to float 0.0
 * daily updates
 * bold new nunits in both desktop and mobile
 * pie chart of bedrooms distributions on /units/bedrooms
 * corrected typo in available template and removed endpoints that don't exist anymore
 * Add lazy highcharts and chart unit prices on the /unit/x/prices page
 * look up unit if enter is hit in unitnumber textfield
 * show floorplan for looked up units
 * daily updates
 * Trying out sublime text
 * Got my head out of my ass and got loadHistoryByNumber working
 * move mobile-app include to mobile.js directive
 * move app.js include to directive in desktop.js
 * Move togglezero handler into controller; try to add lookup for units
 * Allow setting of created_at of price since its used in db:seed
 * Add index to price and unit tables
 * Reindent to 2 spaces
 * Fill out db seed file with delete of all data and reimport using trackladd tasks
 * rename tasks to import...
 * adding unit 2209
 * clean up scaffold created files
 * daily updates
 * change new() to create()
 * Updates to support sencha touch 2.2
 * daily updates
 * correct variable used to set Price's price
 * correctios to price and trend
 * Correct timezone
 * comment out setItems of slideshow
 * Added Trend as a store and view as a way of fixing it
 * Place sass into scss file
 * Rename RentHistory model to Price, create Trend store
 * correct more img urls
 * Got mobileui working with errors, broke up javascript so only loaded on / and /mobile for two uis
 * Add js requires and scss for sencha-touch-rails
 * adding github.com/methyl/sencha-touch-rails to Gemfile
 * adding /mobile page
 * Only create extjs app on home/index
 * redraw graph on completion of store load
 * Clean up and support returning every nth price
 * Adding Missing grid, hidden until unit is missing
 * reduce width of available grid, rotate bottom labels of rent history
 * available grid: flex trend column, adjust image height
 * emptytext for grid
 * Limit prices to last 12 months
 * Show average prices when no unit specified
 * improve queries
 * endpoint for missing units
 * Added bedrooms distribution endpoint to backend, updated frontend url
 * big one: rename renthistory model/store to price; got query for prices working poorly
 * Correct query to average prices
 * Price index is average prices
 * remove linebreaks and print statement from task
 * refactor unit model methods
 * Fix price and date format
 * Add price as attribute of unit
 * first set of changes to support backend
 * Add desktop app to backend; ui shows, no data currently
 * Update title in application template
 * Adding extjs-rails gem
 * Add newrelic
 * Add trend and new to json for unit
 * Add new to unit model
 * Add trend to Unit model
 * Add available method in Units
 * Added home controller
 * Update primary key syntax
 * Added bin to gitignore
 * gemfile.lock
 * Add task that scrapes prices
 * Update gitignore using github rails default
 * Adding httpparty to gemfile
 * renamed fromold to trackladd; added method for importing prices; added link between units and prices
 * task for importing units from old db
 * new rails + models for user and price
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * update gitignore with build and archive
 * update debug.html
 * daily updates
 * Comment out line that throws error
 * tweak
 * Update build.sh
 * Upgrading to Sencha Touch 2.2.1
 * Upgrading to Sencha Touch 2.2.1
 * removing moveinspecial from views in controller
 * daily update
 * Remove unused specials model,store, view
 * daily updates
 * further remove specials since they aren't used any longer
 * update schema file
 * removing backend to refactor first
 * daily updates
 * add prices to show unit json
 * sort prices by date
 * table of prices when showing unit
 * Further corrections to associations, remove need for 3 migrations
 * adding unit-price relationships
 * Followed first steps in guide; created units and prices models; added migrations to support importing data from old db
 * commiting newly created RoR backend
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * LImit rent prices to the last year
 * daily updates
 * point index to ext 4.1.0
 * daily updates
 * Average over week instead of over day
 * daily updates
 * default to not including 0 in rent history
 * daily updates
 * correct capacity to remove model units
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * IDEAS
 * Removing outline around floorplan images
 * Removing outline around floorplan images
 * daily updates
 * crop images to remove sqft/number
 * crop images to remove sqft/number
 * nes carousel view of available units...rent history in cariusel view broken
 * daily updates
 * increase overlay height
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily updates
 * daily update
 * if non-3d, show label
 * add more ideas
 * daily updates
 * Add style->renderer, although I'm not sure how to use it yet
 * dialy updates
 * dialy updates
 * daily updates
 * Cleanup and simplify capture script by using apify.heroku.com
 * remove capturing of specials since there aren't going to be any
 * change badge on refilter; remove completed ideas from ideas files, add in old ones that were lost during upgrade
 * Add panel (starts hidden) that shows floorplan when unit selected
 * Add floorplan files to webui
 * move around charts
 * daily updates
 * review valuegrad from price distro
 * correct how renthistory max is set so all instances are set
 * Revert "set max of rent history to 10% more than highest price in store"
 * 2205 + daily updates
 * set max of rent history to 10% more than highest price in store
 * remove specials enabling since there aren't any
 * daily updates
 * Adding routes
 * hide things that don't work
 * ui tweaks
 * rotate labels
 * iphone view of pie and gauge charts
 * a bunch of chart interactions
 * colorize price distribution with value gradient
 * readd unitdetail to pricedistribution chart
 * show image larger when tapped
 * iphone view
 * improve method for setting unit details
 * daily updates
 * hook up include zero button, but currently not working
 * dialy updates
 * remove Trend and use RentHistory in its place
 * increase max for rent history and trend charts
 * a little controller cleanup, and setting max to be 10% higher than max of store for price distribution chart
 * daily updates
 * Fix RentHistory
 * Fix trend chart
 * Fixed Price Distribution chart
 * cleanup and making gauge a little better looking
 * fixing capacity gauge and adding titlebars
 * comment out broken lines
 * Added alias to store, not sure if it matters
 * Fixed bedroompie
 * Trying to fix Trend chart; shows axes
 * remove watermark
 * comment out setting of store for chart that is currently broken
 * change 'new' type to integer
 * [Major] Upgrading from Sencha touch 2.0.0 to sench touch 2.1; charts currently broken
 * remove guage axes that no longer exists in sencha touch 2.1
 * correct capitalization
 * ideas
 * daily updates
 * add 312
 * daily updates
 * daily updates
 * remove completed idea
 * try to fill in the 6 through 22 floors of units
 * change from hide/show of specials to enable/disable
 * daily updates
 * daily updates + units
 * more trend cleanup
 * daily updates
 * Add buttons for includezero, nonfunctional
 * color best value unit green
 * add Trend.js to build.sh
 * new ideas
 * Replace Trend with renthistory of all
 * Replace Trend with renthistory of all
 * Add yearly trend to mobileui; correct special showing code
 * daily updates
 * Hide specials by default, show if non-empty
 * shorten renthistory chart
 * order mobileui list by price
 * New average unit price graph (trend)
 * daily updates
 * daily updates
 * sort by price
 * Add new button to toggle 0 as the Rent History minimum
 * Move RentHistory chart into a panel within the class
 * Flex price distribution
 * Add unit 907 to db
 * Little less wide, hide specials if empty store
 * Daily update
 * Limit number of returned results for rent history
 * daily updates
 * Add css folder and sass cache to ignore
 * Change to styling icons using css, add price to list
 * perm change
 * perm change
 * add compass compile to build script
 * remove app.css since its compiled
 * Fold specials css into app.scss
 * new units
 * Correct math used to calculate floor that unit is on
 * daily updates
 * Correct setting title of avilable list when there are missing units
 * 516 and daily updates
 * daily updates
 * daily updates and a new unit
 * daily + more units
 * unit update and dailies
 * correct typo
 * daily updates
 * daily updates
 * daily updates, 3 new units
 * increase qtip width
 * Adding more units, more updates
 * adding 614 and a couple days of updates
 * Add logic to capture script to handle price ranges
 * New units and a bunch of daily updates.  This is the first update after the list was empty for a couple days, and it came back with range prices instead of specific prices
 * More units, daily updates
 * daily updtes and a couple new units
 * remove 0 from pricedistribution minimum
 * adding more units and daily updates
 * updates from a few days + a few new units
 * database updates
 * make sql more explicit
 * daily update + 1712, 1906
 * remove rent slider button
 * updates from the last few days
 * Added 1512 and daily update
 * daily update
 * daily update
 * correct new
 * adding 1510
 * remove floorplan from itemdetail panel off price distribution scatter plot
 * remove implemented price filter idea
 * remove pie chart legend, improve itemdetail panel
 * Change available units to use a left join to allow units without a trend, untested
 * update db
 * daily update
 * make sql datetime localtime
 * build file perm
 * correct unit sorting
 * make SQL more native
 * add explicit return false
 * typo
 * containers displaying min/max of slider
 * name rent sliderfield
 * Make slider functional
 * daily update
 * remove console log
 * style trend in price distribution chart using symbols
 * Currently nonfunctional rent range button/slider
 * remove completed idea
 * Color dot red if new
 * Add (new) next to unit numbers that are new
 * file of feature ideas
 * improve query
 * remove old query
 * daily update
 * daily update
 * stuff for doing a minified build
 * rearrange order of tabs
 * daily update
 * daily update
 * Add 'all' filter button and add in controller code
 * Correct unit sorting
 * adding 1207 and 1014
 * Add segmented buttons for filtering bedrooms, not implemented yet
 * add new units
 * added 2216, 1914, 1001
 * Add floorplans to studio/studio+
 * adding 1106 and 1112
 * put floorplan to right of data in template
 * add 1201 and 1902
 * correct title
 * move the navigation view into Available
 * Correct template logic, update ladd db
 * ladd.db
 * new floorplan files, update gitignore
 * move total units into config.Constants
 * add minimum to sqft of price chart
 * panzoom rent history
 * panzoom price chart
 * add iteminfo to rent history
 * Correct renthistory model so chart shows correctly
 * Add crappy line chart to unit details page
 * set title of price chart infoitem
 * correct perms in resources
 * Add value to unitdetails, add unitdetails to iteminfo in price chart
 * move unitdetail template into Constants
 * add floorplan to unit detail
 * improve price distribution axis
 * improve iteminfo for price distribution
 * price scatter plot
 * improve iteminfo
 * Correct capacity guage value
 * correct locaiton of pie interactions
 * Add guage chart
 * Move bedroompie into half of container
 * interactions to pie chart
 * Add rendered labels
 * pie chart of bedrooms
 * Add charts css/js to index.html
 * height of arrows
 * placeholder tabs for charts
 * Correct index title and add title to move in specials tab
 * full year in unitdetails
 * Mark renthistory as remotesort
 * Sort renthistory using DB
 * Add historic prices to unit details page
 * Break unitdetails into 2 containers so I can add in historical prices as well
 * Message when there are no specials, css for specials
 * Badge Units tab with number of available units
 * align images in available list
 * typo, correct bedroom printing logic
 * nonworking code to add asterisk to title if some units are missing
 * Better unit details layout
 * basic data to details page whlie I work on layout
 * got details page to open
 * Correct avaialble xtemplate
 * typo
 * Adding constructor to controller
 * Improve look of gauge chart
 * donut gauge
 * gauge of ladd capacity
 * Trying to xtemplate the list items, buggy itemtap
 * Frist attempt at selecting items in list
 * Remove video
 * Fill out move in special
 * Loads available units into list, sorts
 * Correct urls in proxys
 * config section for models
 * typo
 * Add config section
 * Correct some names
 * Correct model names in stores
 * Adding mobileui...was working...not right now
 * padding and a little wider
 * remove log
 * better labeling
 * expand size of page
 * Clicking on scatter will higlight item on grid
 * Adjust widths and heights
 * Adjust widths and heights
 * css for moveinspecial
 * Add most recent move in special to blank space
 * tighten code for bedroom renderer, add commented out 'w/ den'
 * tooltip of price in renthistory chart
 * RentHistory on select as well as itemclick
 * Pull the number of missing items and add asterisk to available grid title if non zero
 * pull studioplus from db, use in bedrooms column to label
 * Print 'studio' for 0 bedroom units
 * new page for finding units without unit table data
 * Correct bedrooms distributon sql
 * Add price per sqft to price distribution tooltip
 * Add tooltips to trend dots with exact price change
 * correct trend subtraction
 * limit viewport to 1024x768
 * Fix some grid issues
 * Add tip with unit number
 * Improve layout
 * Add grid column with colored circle to represent price change
 * New data for price trend
 * Add count to bargraph
 * Add protection against missing move in special
 * Better layout
 * Interactiveness so clicking item in grid plots its history in chart
 * php, model, store for rent history
 * scatter plot of price per sqft
 * Adding chart of bedroom distribution
 * Web ui with a grid of today's prices
 * code to add scaped data to sqlite db
 * Change schema from sqlitedb instance to text of the schema
 * Adding gitignore
 * Adding db schema
 * Rm database comments
 * initial commit
