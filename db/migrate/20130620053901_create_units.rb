class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units, id: false, primary_key: 'unitnumber' do |t|
      t.integer :unitnumber
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :sqft
      t.boolean :den
      t.boolean :studioplus

      t.timestamps
    end
  end
end
