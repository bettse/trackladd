class AddIndexToUnitsAndPrices < ActiveRecord::Migration
  def change
    change_table :units do |t|
      t.index :unitnumber
    end
    change_table :prices do |t|
      t.index :unitnumber
    end
  end
end
