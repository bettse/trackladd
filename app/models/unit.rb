class Unit < ActiveRecord::Base
  attr_accessible :bathrooms, :bedrooms, :den, :sqft, :studioplus, :unitnumber
  attr_accessor :new, :trend
  self.primary_key = "unitnumber"
  has_many :prices, inverse_of: :unit, foreign_key: "unitnumber", primary_key: 'unitnumber'
  has_one :price, -> { Price.current }, {foreign_key: "unitnumber"}
  has_one :former_price, -> { Price.former }, {class_name: "Price", foreign_key: "unitnumber"}

  scope :available, -> {
    where(unitnumber: Price.current.pluck(:unitnumber))
  }

  #There is a bunch of "price.price" and "former_price.price" stuff in here because
  #there is both a model Price, and it has an attribute 'price'.  This is poor naming in hindsight, where the "Price" should be
  #somethign like "SampledPrice" or "HistoricPrice", or 'price' should be 'value' or 'usd'

  def floorplan
    repeated_floor? ? "x#{unitnumber % 100}".rjust(2, '0') : unitnumber.to_s
  end

  def repeated_floor?
    (5..21).include?((unitnumber.to_i / 100).floor)
  end

  def new
    @new ||= former_price.nil?
  end

  def trend
    @trend ||= new ? 0 : (price.price - former_price.price)
  end

  def as_json(options = {})
    if unitnumber
      super(options || {}).merge({
        price: price.price,
        trend: trend,
        new: new
      })
    else
      super(options)
    end
  end
end
