class Api
  include Scrapify::Base
  #http://www.liveladd.com/inventory/floor_plans/3690/5/7/min_rate
  html "http://units.realtydatatrust.com/UnitAvailability.aspx?ILS=358&propid=29478&tname=ladd&bPage=UT&fVWPage=1&showAllUnits=1&tQuery="

  attribute :unitnumber, css: 'td.unit'
  attribute :bedrooms, css: 'td.bed'
  attribute :bathrooms, css: 'td.bath'
  attribute :squarefoot, css: 'td.sqft'
  attribute :price, css: 'td.price'

  key :unitnumber
end

