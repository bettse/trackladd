class Price < ActiveRecord::Base
  attr_accessible :price, :unitnumber, :created_at #We set created_at when importing
  belongs_to :unit, inverse_of: :prices, foreign_key: "unitnumber", primary_key: 'unitnumber'

  SANE_LIMIT = 1000

  scope :by_date, -> {
    group_by_day(:created_at).
    average(:price).to_a.last(SANE_LIMIT).collect do |record_pair|
      { created_at: record_pair.first, price: record_pair.last == 0 ? nil : record_pair.last.to_i }
    end
  }

  scope :current, -> {
    where("#{table_name}.created_at::date = ?", Date.today.midnight).distinct(:unitnumber)
  }

  scope :former, -> {
    where("#{table_name}.created_at::date = ?", Date.yesterday.midnight)
  }

end
