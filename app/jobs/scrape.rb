class Scrape
  include Sidekiq::Worker

  def perform
    available_types = HTTParty.get('http://www.liveladd.com/inventory/floor_plans/3690/5/7/min_rate')
    type_ids = available_types.collect{|type| type['id']}
    available = type_ids.collect { |type_id| HTTParty.get("http://www.liveladd.com/inventory/child_units/3690/#{type_id}") }.flatten.uniq

    available.each do |update|
      Price.create({unitnumber: update['size'].to_i, price: update['rate'].to_i})
    end
  end
end
