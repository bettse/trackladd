module UnitsHelper

  def format_bedrooms(unit)
    if unit.bedrooms.zero?
      if unit.studioplus
        'Studio+'
      else
        'Studio'
      end
    else
      if unit.den
        "#{unit.bedrooms} w/ den"
      else
        "#{unit.bedrooms}"
      end
    end
  end

  def badge(trend)
    if trend < 0
      'badge badge-success'
    elsif trend > 0
      'badge badge-important'
    end
  end

  def icon(trend)
    if trend < 0
      'icon-white icon-arrow-down'
    elsif trend > 0
      'icon-white icon-arrow-up'
    end
  end
end

