/* global timeScatterChart, d3, $ */

function drawD3ScallerPlot(selection) {
    var formatDate = d3.time.format.iso;
    var moneyFormat = d3.format("$,.2f");
    var color = d3.scale.category10();
    var pricescatter = timeScatterChart()
      .x(function(d) { return formatDate.parse(d.created_at); })
      .y(function(d) { return +d.price; })
      .r(function(d) { return +d.sqft / 200; })
      .c(function(d) { return color(d.bedrooms); })
      .yFormat(moneyFormat)
      .title("Scatterplot of prices through time");

    d3.json("/prices.json", function(error, data) {
        d3.select(selection)
        .datum(data)
        .call(pricescatter);
    });

}

//drawD3ScallerPlot('#d3pricescatter');
