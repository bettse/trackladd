/* global d3, Highcharts, $ */

$(function () {
    //Variables
    var historychart;

    //Configs
    var valuechartoptions = {
        chart: {
            renderTo: 'valuechart',
            type: 'scatter',
            zoomType: 'xy'
        },
        series: [{}],
        title: {
            text: "Price per sqft"
        },
        plotOptions: {
            series: {
                point: {
                    events: {
                        click: function(e) {
                            valuepointclick(this, e);
                        }
                    }
                }
            }
        },
        legend: false,
        tooltip: {
            formatter: function() {
                var price = Highcharts.numberFormat(this.point.y/this.point.x, 2);
                return 'Unit #' + this.point.name + '<br /> $' + price + '/sqft';
            }
        },
        yAxis: {
            labels: {
                formatter: function() {
                    return '$' + Highcharts.numberFormat(this.value, 0);
                }
            }
        }
    };

    var pricehistoryoptions = {
        chart: {
            renderTo: 'pricehistory',
            type: 'line',
            zoomType: 'x'
        },
        title: {
            text: "Price History"
        },
        xAxis: {
            ordinal: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>'
        },
        rangeSelector: {
            selected: 2 //Start with 6mo zoom
        },
        plotOptions: {
            series: {
                turboThreshold: 0
            }
        }
    };

    //Listeners
    $('#toggleZero').click(function(e) {
        var chart = $('#history').highcharts();
        if ($(e.target).hasClass('active')) {
            chart.yAxis[0].setExtremes(null, null);
        } else {
            chart.yAxis[0].setExtremes(0, null);
        }
    });

    $("#all").click(function(e) {
        loadhistory("/prices/average", $(this).text());
    });
    $("#studios").click(function(e) {
        loadhistory("/prices/0/bedrooms", $(this).text());
    });
    $("#ones").click(function(e) {
        loadhistory("/prices/1/bedrooms", $(this).text());
    });
    $("#twos").click(function(e) {
        loadhistory("/prices/2/bedrooms", $(this).text());
    });
    $("#loadunit").click(function(e) {
        loadunithistory($('#unitnumber').val());
    });

    //Funcions
    var valuepointclick = function(point, e) {
        loadunithistory(point.name);
    };

    var loadunithistory = function(unitnumber) {
        loadhistory('/units/' + unitnumber, unitnumber);
    };

    var loadhistory = function(url, display) {
        var remappeddata;
        if($('#pricehistory').length > 0){
            $.getJSON(url, function(data) {
                var remappeddata = $.map(data, function(price){
                    return {'x': new Date(price.created_at).getTime(), 'y': price.price && +price.price};
                });
                if (historychart) {
                    historychart.destroy();
                }
                pricehistoryoptions.title.text = "Price History for " + display;
                pricehistoryoptions.series = [{name: 'Price', data: remappeddata}];
                historychart = new Highcharts.StockChart(pricehistoryoptions);
            });
        }
    };

    if($('#valuechart').length > 0){
        var remappeddata;
        $.getJSON("/units",function(data) {
            remappeddata = $.map(data, function(unit){
                return {'name': unit.unitnumber, 'x': unit.sqft, 'y': unit.price};
            });
            valuechartoptions.series = [
                {
                    data: remappeddata,
                    type: 'scatter'
                }
            ];
            if (remappeddata.length > 0) {
                var chart = new Highcharts.Chart(valuechartoptions);
            }
        });

        $.getJSON("/units?regression=true",function(data) {
            valuechartoptions.series = [{
                data: data.regression,
                type: 'line',
                marker: {
                    enabled: false
                },
                enableMouseTracking: false
            }];
        });
    }

});

