/* globals angular */
var app = angular.module('TrackLadd');
app.factory('Unit', ["$resource", function($resource) {
  var resource = $resource('/units/:id', null, {
    'get': {isArray: true}
  });
  return resource;
}]);
