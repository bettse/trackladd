/* globals angular, $ */
angular.module('TrackLadd', ['ngRoute', 'templates', 'ngResource', 'trackladd.filters', 'ui.bootstrap', 'asset-path'])
  .config(["$httpProvider", "$locationProvider", "$routeProvider", function ($httpProvider, $locationProvider, $routeProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    $httpProvider.defaults.headers.common.Accept = "application/json";

    $routeProvider
      .when('/', {
        templateUrl: 'units.html',
        controller: 'UnitsCtrl'
    });

    $locationProvider.html5Mode(true);
}]);
