/* globals angular, console, Highcharts */
var app = angular.module('TrackLadd');
app.controller('UnitsCtrl', function ($scope, Unit, $uibModal) {
    Unit.query(function(data) {
      $scope.units = data;
    });

    var pricehistoryoptions = {
        chart: {
            renderTo: 'pricehistory',
            type: 'line',
            zoomType: 'x'
        },
        title: {
            text: "Price History"
        },
        xAxis: {
            ordinal: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>'
        },
        rangeSelector: {
            selected: 2 //Start with 6mo zoom
        }
    };


    $scope.unitHistory = function() {
      var unit = this.unit;
      Unit.get({id: unit.unitnumber}, function(prices) {
          var remappeddata = prices.map(function(price){
              return {'x': new Date(price.created_at).getTime(), 'y': price.price && +price.price};
          });

          if ($scope.historychart) {
              $scope.historychart.destroy();
          }
          pricehistoryoptions.title.text = "Price History for " + unit.unitnumber;
          pricehistoryoptions.series = [{name: 'Price', data: remappeddata}];
          $scope.historychart = new Highcharts.StockChart(pricehistoryoptions);
      });
    };

    $scope.unitinfo = function() {
      var unit = this.unit;

      var uibModalInstance = $uibModal.open({
        templateUrl: 'unitinfo.html',
        controller: 'UnitInfo',
        resolve: {
          unit: function() {
            return unit;
          }
        }
      });

      uibModalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
    };
});
