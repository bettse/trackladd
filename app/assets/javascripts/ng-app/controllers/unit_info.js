/* global angular */
var app = angular.module('TrackLadd');
app.controller('UnitInfo', function ($scope, $uibModalInstance, unit) {

  $scope.unit = unit;

  $scope.ok = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
  };

});
