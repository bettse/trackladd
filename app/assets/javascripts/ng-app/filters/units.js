/* globals angular */
angular.module('trackladd.filters', [])
    .filter('floorplan', function(assetPath) {
        return function(unit) {
          var floor = Math.floor(unit.unitnumber / 100);
          var filename;
          if (floor > 5 && floor < 22) {
            filename = 'x' +  String("0" + unit.unitnumber % 100).slice(-2);
          } else {
            filename = '' + unit.unitnumber;
          }
          return assetPath('floorplans/' + filename + '.png');
        };
    })
    .filter('format_bedrooms', function() {
        return function(unit) {
          if (+unit.bedrooms === 0) {
            if (unit.studioplus) {
              return "Studio+";
            } else {
              return "Studio";
            }
          } else {
            if (unit.den) {
              return String(unit.bedrooms) + " w/ den";
            } else {
              return String(unit.bedrooms);
            }
          }
          return '';
        };
    })
    .filter('unit_class', function() {
        return function(unit) {
          if (unit['new']) {
            return "info";
          }
        };
    })
    .filter('trend_icon', function() {
        return function(trend) {
          var icon = 'icon-white ';
          if (trend < 0) {
            icon += 'icon-arrow-down';
          } else if (trend > 0) {
            icon += 'icon-arrow-up';
          }
          return icon;
        };
    })
    .filter('trend_badge', function() {
        return function(trend) {
          var badge = 'badge ';
          if (trend < 0) {
            badge += 'badge-success';
          } else if (trend > 0) {
            badge += 'badge-important';
          } else {
            return '';
          }
          return badge;
        };
    });
