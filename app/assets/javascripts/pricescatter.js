
function timeScatterChart() {
    var margin = {top: 30, right: 20, bottom: 100, left: 50},
    width = 970 - margin.left - margin.right,
    height = 700 - margin.top - margin.bottom,
    yFormat = d3.format("g"),
    xValue = function(d) { return d[0]; },
    yValue = function(d) { return d[1]; },
    rValue = function(d) { return d[2]; },
    cValue = function(d) { return d[3]; },
    xScale = d3.time.scale(),
    yScale = d3.scale.log(),
    xAxis = d3.svg.axis().scale(xScale).orient("bottom").ticks(12),
    yAxis = d3.svg.axis().scale(yScale).orient("left").tickFormat(yFormat),
    title = false;

    function chart(selection) {
        selection.each(function(data) {
            width = $(this).width();

            // Convert data to standard representation greedily;
            // this is needed for nondeterministic accessors.
            data = data.map(function(d, i) {
                return [xValue.call(data, d, i), yValue.call(data, d, i), rValue.call(data, d, i), cValue.call(data, d, i)];
            });

            // Update the x-scale.
            xScale
            .domain(d3.extent(data, function(d) { return d[0]; }))
            .range([0, width - margin.left - margin.right]);

            // Update the y-scale.
            yScale
            .domain(d3.extent(data, function(d) { return d[1]; }))
            .range([height, 0]);

            // Select the svg element, if it exists.
            var svg = d3.select(this).selectAll("svg").data([data]);

            // Otherwise, create the skeletal chart.
            var gEnter = svg.enter().append("svg").append("g");
            gEnter.append("g").attr("class", "x axis");
            gEnter.append("g").attr("class", "y axis");


            // Update the outer dimensions.
            svg
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom);

            // Update the inner dimensions.
            var g = svg.select("g")
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // Add dots.
            g.selectAll(".dot")
              .data(data)
              .enter().append("circle")
              .attr("class", "dot")
              .attr("cx", X)
              .attr("cy", Y)
              .attr("r", function(d) { return d[2]; })
              .style("fill", function(d) { return d[3]; });


            // Update the x-axis.
            g.select(".x.axis")
              .attr("transform", "translate(0," + yScale.range()[0] + ")")
              .call(xAxis)
              .selectAll("text")
              .style("text-anchor", "end")
              .attr("dx", "-.8em")
              .attr("dy", ".15em")
              .attr("transform", function(d) {
                  return "rotate(-65)";
              });

            // Update the y-axis
            g.select(".y.axis")
              .call(yAxis)
              .append("text")
              .attr("transform", "rotate(-90)")
              .attr("y", 6)
              .attr("dy", ".71em")
              .style("text-anchor", "end");

            if(title) {
                g.append("text")
                  .attr("class", "title")
                  .attr("x", (width / 2))
                  .attr("y", 0 - (margin.top / 2))
                  .attr("text-anchor", "middle")
                  .text(title);
            }
        });
    }

    // The x-accessor for the path generator; xScale xValue.
    function X(d) {
        return xScale(d[0]);
    }

    // The y-accessor for the path generator; yScale yValue.
    function Y(d) {
        return yScale(d[1]);
    }

    chart.margin = function(_) {
        if (!arguments.length) return margin;
        margin = _;
        return chart;
    };

    chart.yFormat = function(_) {
        if (!arguments.length) return yFormat;
        yFormat = _;
        return chart;
    };

    chart.width = function(_) {
        if (!arguments.length) return width;
        width = _;
        return chart;
    };

    chart.height = function(_) {
        if (!arguments.length) return height;
        height = _;
        return chart;
    };

    chart.x = function(_) {
        if (!arguments.length) return xValue;
        xValue = _;
        return chart;
    };

    chart.y = function(_) {
        if (!arguments.length) return yValue;
        yValue = _;
        return chart;
    };

    chart.r = function(_) {
        if (!arguments.length) return rValue;
        rValue = _;
        return chart;
    };

    chart.c = function(_) {
        if (!arguments.length) return cValue;
        cValue = _;
        return chart;
    };

    chart.title = function(_) {
        if (!arguments.length) return title;
        title = _;
        return chart;
    };
    return chart;
}
