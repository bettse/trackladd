class UnitsController < ApplicationController

  def index
    respond_to do |format|
      format.html { render }
      format.json do
        @units = Unit.available.includes(:price).includes(:former_price)
        @units = @units.sort_by(&:price)
        render json: @units
      end
    end
  end

  def linear_regression
    if @units.any?
      xs = @units.map(&:sqft).sort
      ys = @units.map(&:price).sort
      linear_model = SimpleLinearRegression.new(xs, ys)
      logger.debug "Estimated Linear Model: Y = #{linear_model.y_intercept} + (#{linear_model.slope} * X)"
      [
        {x: xs.first, y: linear_model.y_intercept + (linear_model.slope * xs.first)},
        {x: xs.last, y: linear_model.y_intercept + (linear_model.slope * xs.last)}
      ]
    end
  end

  # GET /units/1
  # GET /units/1.json
  def show
    @unit = Unit.find(params[:id])
    respond_to do |format|
      format.html { request.xhr? ? render(partial: 'show') : render }
      format.json { render json:@unit.prices.by_date }
    end
  end

  def self.ladd_capacity
    329
  end
end
