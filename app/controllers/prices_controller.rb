class PricesController < ApplicationController
  def index
    @prices = Price.joins(:unit).select(:unitnumber, :bedrooms, :price, :created_at, :sqft)
    respond_to do |format|
      format.json do
        render json: @prices
      end
    end
  end

  def average
    @prices = Price.by_date
    respond_to do |format|
      format.json { render json: @prices }
    end
  end

  def bedrooms
    if (params.key?(:number))
      @units = Unit.where(bedrooms: params[:number]).to_a
      @prices = Price.where(unitnumber: @units).by_date
    else
      average_all
    end
    respond_to do |format|
      format.json { render json: @prices }
    end
  end
end
