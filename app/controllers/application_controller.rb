class ApplicationController < ActionController::Base
  protect_from_forgery

  #The ladd flash requests a views.xml and I want to prevent this from being a 404
  def views
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render xml: '' }
    end
  end
end
