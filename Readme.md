
## Project to track rent prices

[![Code Climate](https://codeclimate.com/github/bettse/trackladd.png)](https://codeclimate.com/github/bettse/trackladd)

To update prices when the timed job fails, use `http PUT http://trackladd.ericbetts.org/prices/task`

##Ideas
  * Upgrade AngularJS
  * Use date table to create nils for unavailable dates
  * Could a single nil cause a break in the highchart line?
  * Table sorting (ng)
  * db/seeds file of units
  * Hi resolution floorplan images


##Rake stats

+----------------------+-------+-------+---------+---------+-----+-------+
| Name                 | Lines |   LOC | Classes | Methods | M/C | LOC/M |
+----------------------+-------+-------+---------+---------+-----+-------+
| Controllers          |   134 |   112 |       3 |      10 |   3 |     9 |
| Helpers              |    42 |    38 |       0 |       4 |   0 |     7 |
| Models               |    88 |    71 |       3 |       8 |   2 |     6 |
| Mailers              |     0 |     0 |       0 |       0 |   0 |     0 |
| Javascripts          |   536 |   123 |       0 |      24 |   0 |     3 |
| Libraries            | 27590 | 10450 |       1 |    1420 | 1420 |     5 |
| Integration tests    |     0 |     0 |       0 |       0 |   0 |     0 |
| Functional tests (old) |   107 |    85 |       3 |       0 |   0 |     0 |
+----------------------+-------+-------+---------+---------+-----+-------+
| Total                | 28497 | 10879 |      10 |    1466 | 146 |     5 |
+----------------------+-------+-------+---------+---------+-----+-------+
  Code LOC: 10794     Test LOC: 85     Code to Test Ratio: 1:0.0

